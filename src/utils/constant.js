export default {
  baseURL: "http://localhost:8081",
  imBaseURL: "http://localhost:81",
  webURL: "http://localhost",

  // baseURL: "https://poetize.cn/api",
  // imBaseURL: "https://poetize.cn/im",
  // webURL: "https://poetize.cn",

  live2d_path: "https://cdn.jsdelivr.net/gh/stevenjoezhang/live2d-widget@latest/",
  cdnPath: "https://cdn.jsdelivr.net/gh/fghrsh/live2d_api/",
  waifuPath: "/webInfo/getWaifuJson",
  hitokoto: "https://v1.hitokoto.cn",
  tocbot: "https://cdnjs.cloudflare.com/ajax/libs/tocbot/4.11.1/tocbot.min.js",
  jinrishici: "https://v1.jinrishici.com/all.json",
  random_image: "$$$$随机图片",
  //前后端定义的密钥，AES使用16位
  cryptojs_key: "$$$$密码加密传输",
  qiniuUrl: "https://upload.qiniup.com",
  qiniuDownload: "$$$$七牛云下载地址",

  friendBG: "$$$$图片地址",
  friendLetterTop: "$$$$图片地址",
  friendLetterBottom: "$$$$图片地址",
  friendLetterBiLi: "$$$$图片地址",
  friendLetterMiddle: "$$$$图片地址",

  before_color_list: ["$$$$随机颜色"],

  tree_hole_color: ["$$$$随机颜色"],

  two_poem_image: ["$$$$图片地址"],

  before_color_1: "black",
  after_color_1: "linear-gradient(45deg, #f43f3b, #ec008c)",

  before_color_2: "rgb(131, 123, 199)",
  after_color_2: "linear-gradient(45deg, #f43f3b, #ec008c)",

  pageColor: "#ee7752",
  commentPageColor: "#23d5ab",
  userId: 1,
  source: 0,

  emojiList: ['衰', '鄙视', '再见', '捂嘴', '皱眉', '奋斗', '白眼', '可怜', '皱眉', '鼓掌', '烦恼', '吐舌', '挖鼻', '委屈', '滑稽', '啊这', '生气', '害羞', '晕', '好色', '流泪', '吐血', '微笑', '酷', '坏笑', '吓', '大兵', '哭笑', '困', '呲牙']
}
